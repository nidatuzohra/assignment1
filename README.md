To run this app, clone this repo to your local machine. Then click on the .exe file to run the app. 

Note: MIT License allows for commercial use, modifications, distribution and private use. Hence, this repo is copyrighted under MIT License. 

Edit: An article is added to the wiki that discusses repo privacy and open-source software. 